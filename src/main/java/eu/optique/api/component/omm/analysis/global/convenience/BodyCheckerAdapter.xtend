/**
 * 
 */
package eu.optique.api.component.omm.analysis.global.convenience

import eu.optique.api.component.omm.analysis.impl.bodychecker.BodyChecker
import eu.optique.api.mapping.TriplesMap
import eu.optique.config.OptiqueConfig
import java.util.Map

import static extension eu.optique.api.component.omm.analysis.global.MappingAnalysisTools.*
import static eu.optique.api.component.omm.analysis.global.convenience.Statics.*
import static eu.optique.api.component.omm.analysis.global.convenience.Globals.*
import static extension eu.optique.api.component.omm.analysis.global.convenience.Extensions.*

/**
 * @author jmora
 * 
 */
class BodyCheckerAdapter {
	private static val proverConfigPath = if(!jmora || "config.prop".exists) OptiqueConfig.config.eproverPath else null
	private static val plausibleLocations = #["/public/theoremprover/eprover/eprover"]
	private static val lastAttempt = "resources/TheoremProver/Eprover/./eprover"
	private static val fallback = plausibleLocations.filter[exists].headOrElse(lastAttempt)
	private static val axioms = '''fof(ref1,axiom,(![X] : ( lesseq(X,X) ))).
fof(transless,axiom,(![X,Y,Z] : ((less(X,Y) & less(Z,X)) => less(Z,Y)))).
fof(antisim,axiom,(![X,Y] : ((lesseq(X,Y) & lesseq(Y,X)) => (X=Y)))).
fof(irrefl1,axiom,(![X] : ~( less(X,X) ))).
fof(asym,axiom,(![X,Y] : (less(X,Y) => ~less(Y,X)))).
fof(dual1,axiom,(![X,Y] : (less(X,Y) <=> greater(Y,X)))).
fof(dual2,axiom,(![X,Y] : (lesseq(X,Y) <=> greatereq(Y,X)))).
fof(refred,axiom,(![X,Y] : (less(X,Y) <=> (lesseq(X,Y) & (X != Y))))).
'''

	private val BodyChecker bodyChecker
	private static val logger = org.apache.log4j.Logger.getLogger(BodyCheckerAdapter.name)

	new(String schemata) {
		val proverPath = if(!jmora || proverConfigPath?.exists) proverConfigPath else fallback
		bodyChecker = if (!jmora || proverPath?.exists)
			new BodyChecker => [
				theoremProverPath = proverPath
				schema = schemata
				axiomsPath = axioms.dumpFile
				filePath = "".dumpFile
				memorylimit = 1024
				timeout = 30
			]
		else
			new BodyCheckerStub()

	}

	synchronized def getBodyConsistency(TriplesMap tm) {
		Maybe[bodyChecker.checkBodyConsistency(tm)] ?: otherwise(tm)
	}

	synchronized def bodySubsumes(TriplesMap tm1, TriplesMap tm2) {
		tm1.renamings(tm2).map[asMap].exists [
			Maybe[bodyChecker.checkBodySubsumption(tm2, tm1, it)] ?: otherwise(tm2, tm1, it)
		]
	}

	def otherwise(TriplesMap map) {
		logger.warn('''Exception when checking consistency for "«map.name»", assuming consistency.''')
		true
	}

	def otherwise(TriplesMap map, TriplesMap map2, Map<String, String> map3) {
		logger.
			warn('''Exception checking subsumption for «map.name» («map.query») and «map2.name» («map2.query») with «map3.pretty», assuming not subsumed.''')
		false
	}

	private def getPretty(Map<String, String> map) {
		'{' + map.entrySet.map[key + ': ' + value].join(', ') + '}'
	}

	static private def getQuery(TriplesMap tm) {
		tm.logicalTable.SQLQuery
	}

	synchronized def projectionSubsumes(TriplesMap tm1, TriplesMap tm2, Map<String, String> unification) {
		Maybe[bodyChecker.checkBodySubsumption(tm2, tm1, unification.invert)] ?: otherwise(tm2, tm1, unification.invert)
	}

}