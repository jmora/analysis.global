/**
 * 
 */
package eu.optique.api.component.omm.analysis.global

/**
 * @author jmora
 * 
 */
class ConstantBuilder {
	private Long next

	new() {
		next = 1l
	}

	synchronized def getNext() {
		val name = toName(next)
		next++
		(0 ..< (8 - name.length)).map["a"].join + name
	}

	private static def toName(Long value) {
		val res = new StringBuffer
		var pending = value
		val char a = 'a'
		val char z = 'z'
		val diff = z - a + 1
		while (pending >= diff) {
			res.append((pending % diff + a) as char)
			pending = pending / diff
		}
		res.append((pending + a) as char)
		res.reverse.toString
	}
}