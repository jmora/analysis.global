/**
 * 
 */
package eu.optique.api.component.omm.analysis.global.interfaces

import org.eclipse.xtend.lib.annotations.Accessors

/**
 * @author jmora
 *
 */
class MappingAnalysisCode {
	
	@Accessors(PUBLIC_GETTER) val String msg
	
	private new (String message){
		msg = message
	}
	
	private static def Code(String code){
		new MappingAnalysisCode(code)
	}
	
	override toString(){
		msg
	}
	
	public static val headSubsumes = Code("mapping head subsumes mappings: ")
	public static val bodySubsumes = Code("mapping body subsumes mappings: ")
	public static val fullSubsumes = Code("mapping subsumes mappings: ")
	public static val headInconsistency = Code("mapping head is not consistent")
	public static val bodyInconsistency = Code("mapping body is not consistent")
	public static val mappingInconsistency = Code("mapping is not consistent")
	public static val globalRedundancyCode = Code("mapping globally redundant with:")
	public static val globalInconsistency = Code("mappings are not globally consistent")
}