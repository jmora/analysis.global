/**
 * 
 */
package eu.optique.api.component.omm.analysis.global

import com.fluidops.iwb.datasource.metadata.Schema
import com.google.common.io.Files
import eu.optique.api.component.omm.analysis.global.interfaces.MappingAnalysisCode
import eu.optique.api.component.omm.analysis.global.interfaces.SimpleSerializable
import eu.optique.api.mapping.ManageResource
import eu.optique.api.mapping.Template
import eu.optique.api.mapping.TermMap
import eu.optique.api.mapping.TriplesMap
import java.net.URI
import java.util.ArrayList
import java.util.Collection
import java.util.List
import java.util.Map
import org.apache.commons.codec.Charsets
import org.apache.log4j.Logger
import org.eclipse.xtext.xbase.lib.Functions.Function1
import org.eclipse.xtext.xbase.lib.Functions.Function2
import org.openrdf.model.Resource
import org.semanticweb.owlapi.model.OWLAxiom
import org.eclipse.xtend.lib.annotations.Data

import static com.fluidops.iwb.api.EndpointImpl.*
import static eu.optique.api.component.omm.analysis.convenience.Globals.*
import static eu.optique.api.component.omm.analysis.global.convenience.Globals.*

import static extension eu.optique.api.component.omm.analysis.global.convenience.Extensions.*

/**
 * @author jmora
 * 
 */
class MappingAnalysisTools {

	private static val logger = Logger.getLogger(MappingAnalysisTools.name)

	static def getSignature(List<Schema> schemata) {
		val start = System.currentTimeMillis
		logger.info("Getting the signature from the schema...")
		val r = schemata.map [ schema |
			schema.tables.pmap [ table |
				schema.name + "." + table.name -> table.columns.map[name]
			]
		].flatten.asMap
		logger.info('''Got the signature from the schema, took «System.currentTimeMillis - start»ms''')
		r
	}

	static def getSchemaFile(Map<String, List<String>> dbsignature) {
		dbsignature.schemaString.dumpFile
	}

	static def getSchemaString(Map<String, List<String>> dbsignature) {
		dbsignature.entrySet.map[key + "(" + value.join(",") + ")"].join("\n")
	}

	static def getSchemaMap(String filePath) {
		// TODO: consider here that maybe schema names cause trouble 
		Files.readLines(filePath.toFile, Charsets.UTF_8).map[it.split("\\(")].toMap[get(0).split('\\.').last].mapValues [
			get(1).split('\\)').get(0).split(',').map[trim]
		].immmap
	}

	static def traverse(List<?> traversed) {
		matrixFancyTraversal(traversed, false)
	}

	static def fullTraverse(List<?> traversed) {
		matrixFancyTraversal(traversed, true)
	}

	private static def matrixFancyTraversal(List<?> traversed, boolean diagonal) {
		val s = traversed.size
		val h = s / 2
		val m = (s - 1) / 2
		val d = if(diagonal) 0 else 1
		val part1 = (0 ..< h).map[i|(i + d .. i + h).map[i -> it]]
		val part2 = (h ..< s).map[i|(i + d .. i + m).map[it % s].map[i -> it]]
		(part1 + part2).flatten
	}

	private static def <T, R> innerMatrixBuild(List<T> traversed, Function2<T, T, Pair<R, R>> f,
		Iterable<Pair<Integer, Integer>> order) {
		val s = traversed.size
		val List<List<R>> matrix = (0 ..< s).map[i|(0 ..< s).map[null].toArrayList].asList
		order.map[new InnerResult<T, R>(traversed, f, key, value)].forEach [
			if (column != row)
				matrix.get(column).set(row, backward)
			matrix.get(row).set(column, forward)
		]
		matrix
	}

	private static class InnerResult<T, R> {
		Integer row
		Integer column
		R forward
		R backward

		new(List<T> traversed, Function2<T, T, Pair<R, R>> f, Integer row, Integer column) {
			this.row = row
			this.column = column
			val r = f.apply(traversed.get(row), traversed.get(column))
			this.forward = r.key
			this.backward = r.value
		}
	}

	static def <T, R> matrixBuild(List<T> traversed, Function2<T, T, Pair<R, R>> f) {
		innerMatrixBuild(traversed, f, traverse(traversed))
	}

	static def <T, R> fullMatrixBuild(List<T> traversed, Function2<T, T, Pair<R, R>> f) {
		innerMatrixBuild(traversed, f, fullTraverse(traversed))
	}

	static def List<String> getSegments(Template t) {
		val List<String> r = #[]
		var niceSoftwareIndeed = true
		for (var i = 0; niceSoftwareIndeed; i++)
			try {
				r.add(t.getStringSegment(i))
			} catch (Exception e) {
				niceSoftwareIndeed = false
			}
		r
	}

	private static def function(TermMap t) {
		t.template.segments.join("##")
	}

	static def List<Pair<String, String>> unify(TermMap t1, TermMap t2) {
		if (t1.termMapType != t2.termMapType)
			return #[]
		switch (t1.termMapType) {
			case COLUMN_VALUED: #[t1.column -> t2.column]
			case TEMPLATE_VALUED: if(t1.function == t2.function) t1.variables.zip(t2.variables).asList else #[]
			default: #[]
		}
	}

	private static def isCompatible(TermMap t1, TermMap t2) {
		if (t1.termMapType != t2.termMapType)
			false
		else
			switch (t1.termMapType) {
				case COLUMN_VALUED: true
				case TEMPLATE_VALUED: t1.function == t2.function
				case CONSTANT_VALUED: t1.constant == t2.constant
				default: false
			}
	}

	static def getName(extension TriplesMap tm) {
		try {
			sesameResource.resource.localName.toString
		} catch (Exception e) {
			sesameAnonymousResource.resource.toString
		}
	}

	static def getHREF(TriplesMap tm) {
		val rsr = tm.asSesame
		val label = ( 
		try {
			api.dataManager.getLabel(rsr)
		} catch (Exception e) {
			"/resource/?uri=" + rsr.toString
		})
		val tooltip = "Click to open mapping rule in new tab."
		api.requestMapper.getAHrefEncoded(rsr, label, tooltip, #{'target' -> '_blank'})
	}

	static def getURI(extension TriplesMap tm) {
		URI.create(try {
			sesameResource.resource.toString // sesameResource.resource.namespace + sesameResource.resource.localName
		} catch (Exception e) {
			bnodeprefix + sesameAnonymousResource.resource.ID
		})
	}

	static def Resource asSesame(extension ManageResource resource) {
		try {
			sesameResource.resource
		} catch (Exception e) {
			sesameAnonymousResource.resource
		}
	}

	private static def objectTerms(TriplesMap tm) {
		tm.predicateObjectMaps.map[objectMaps].flatten
	}

	private static def combine(Iterable<List<Pair<String, String>>> unifiers1,
		Iterable<List<Pair<String, String>>> unifiers2) {
		unifiers1.filter[!isEmpty].map[u1|unifiers2.filter[!isEmpty].map[u2|(u1 + u2).toList]].flatten
	}

	static def getVariables(TriplesMap tm) {
		(tm.subjectMap.variables + tm.objectTerms.map[variables].flatten).asSet
	}

	static def getVariables(TermMap tm) {
		switch (tm.termMapType) {
			case COLUMN_VALUED: #[tm.column]
			case TEMPLATE_VALUED: tm.template.columnNames
			default: #[]
		}
	}

	static def <K, T> Result(List<K> s, MappingAnalysisCode p, List<T> o) {
		new SimpleSerializable(s, p, o)
	}

	static def isGloballyConsistent(Iterable<OWLAxiom> axioms) {
	}

	// Iterable of unifiers
	// a unifier is a list of unifications
	// a unification is a pair linking one variable in tm1 with a variable in tm2 
	static def Iterable<List<Pair<String, String>>> partialUnifications(TriplesMap tm1, TriplesMap tm2) {
		val s1s2 = #[tm1.subjectMap.unify(tm2.subjectMap)]
		val s1o2 = tm2.objectTerms.map[tm1.subjectMap.unify(it)]
		val o1s2 = tm1.objectTerms.map[unify(tm2.subjectMap)]
		val o1o2 = tm1.objectTerms.map[a|tm2.objectTerms.map[b|a.unify(b)]].flatten()
		val candidates = (s1s2 + s1o2 + o1s2 + o1o2 + s1s2.combine(o1o2) + s1o2.combine(o1s2))
		candidates.filter[!isEmpty].map[asSet.asList].filter[map[key].asSet.size == map[value].asSet.size == size]
	}

	// This is the queens problem, very boring.
	static def Iterable<List<Pair<String, String>>> renamings(TriplesMap tm1, TriplesMap tm2) {
		val vs1 = tm2.variables.asList
		val vs2 = tm2.variables.asList
		if (vs1.size != vs2.size)
			#[]
		else
			eu.optique.api.component.omm.analysis.global.MappingAnalysisTools.backtrack(#[rqps(#[],vs1,vs2)],[rqchildren],[left.isEmpty && right.isEmpty]).map[result]				
	}

	static def <A> backtrack(Iterable<A> initial, Function1<A, Collection<A>> children, Function1<A, Boolean> isFinal) {
		val candidates = initial.asArray
		val solutions = new ArrayList<A>()
		while (!candidates.isEmpty) {
			val candidate = candidates.remove(0)
			if (isFinal.apply(candidate))
				solutions.add(candidate)
			else
				candidates.addAll(0, children.apply(candidate))
		}
		solutions
	}
	
	private static def Collection<RenamingQueens> rqchildren(RenamingQueens ps){
		val nleft = ps.left.asArray
		val v = nleft.remove(0)
		ps.right.map[
			val nright = ps.right.asArray
			val nresult = ps.result.asArray
			nresult.add(v -> it)
			nright.remove(it)
			rqps(nresult.asList, nleft.asList, nright.asList)
		]
	}

	private static def rqps(List<Pair<String, String>> result, List<String> left, List<String> right) {
		new RenamingQueens(result, left, right)
	}

	@Data private static class RenamingQueens {
		val List<Pair<String, String>> result
		val List<String> left
		val List<String> right
	}
}

