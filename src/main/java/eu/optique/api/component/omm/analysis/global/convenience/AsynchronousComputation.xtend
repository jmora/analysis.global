/**
 * 
 */
package eu.optique.api.component.omm.analysis.global.convenience

import java.util.concurrent.Callable
import java.util.concurrent.Future

import static eu.optique.api.component.omm.analysis.global.convenience.Statics.*

/**
 * @author jmora
 * 
 */
class AsynchronousComputation<T> {
	private val Future<T> future
	private val LazyComputation<T> lazy

	new(Callable<T> closure) {
		future = if(Globals.eager) Future(closure)
		lazy = if(!Globals.eager) Lazy(closure) 
	}

	def get() {
		lazy?.get ?: future?.get
	}
}