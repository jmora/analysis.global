package eu.optique.api.component.omm.analysis.global

import com.bitbucket.jmora.xtend.annotations.Extract
import eu.optique.api.component.omm.analysis.global.convenience.LazyComputation
import eu.optique.api.component.omm.analysis.interfaces.optique.HTMLSerializable
import eu.optique.api.component.omm.analysis.syntax.SyntacticAnalyserImpl
import eu.optique.api.mapping.TriplesMap
import java.net.URI
import java.util.ArrayList
import java.util.List
import java.util.Map
import java.util.Set
import org.semanticweb.owlapi.model.OWLAxiom
import org.semanticweb.owlapi.model.OWLOntology
import eu.optique.api.component.omm.analysis.global.interfaces.SimpleSerializable
import static eu.optique.api.component.omm.analysis.global.interfaces.MappingAnalysisCode.*
import static eu.optique.api.component.omm.analysis.global.convenience.Statics.*
import static extension eu.optique.api.component.omm.analysis.global.MappingAnalysisTools.*
import static extension eu.optique.api.component.omm.analysis.global.convenience.AuxiliaryFunctions.*
import static extension eu.optique.api.component.omm.analysis.global.convenience.Extensions.*

@Extract class MappingAnalysisResultImpl {

	private final Set<OWLAxiom> axioms
	private final List<TriplesMap> mappings
	private final SyntacticAnalyserImpl syntactic
	private final String schema
	private final ArrayList<MappingAssertionAnalyser> mappingAnalysers
	private final Map<String, Integer> mappingAnalysersIndexes
	private final LazyComputation<List<List<Boolean>>> headSubsumptionMatrix
	private final LazyComputation<List<List<Boolean>>> bodySubsumptionMatrix
	private final LazyComputation<List<List<Set<OWLAxiom>>>> abox
	private final LazyComputation<Boolean> globalConsistency
	private final LazyComputation<List<Boolean>> globalRedundancy

	new(OWLOntology ontology, List<TriplesMap> mappings, SyntacticAnalyserImpl syntacticAnalyser, String dbSchema) {
		this.axioms = ontology.axioms
		this.mappings = mappings
		this.syntactic = syntacticAnalyser
		this.schema = dbSchema
		val ops = ontology.objectPropertiesInSignature.map[IRI.toString].asSet
		this.mappingAnalysers = mappings.pmap[new MappingAssertionAnalyser(it, axioms, schema, syntactic, ops)].asArray
		this.mappingAnalysersIndexes = (0 ..< mappingAnalysers.size).toMap[mappingAnalysers.get(it).URI.toString]
		this.headSubsumptionMatrix = Lazy[mappingAnalysers.matrixBuild[a, b|a.getHeadSubsumption(b)]]
		this.bodySubsumptionMatrix = Lazy[mappingAnalysers.matrixBuild[a, b|a.getBodySubsumption(b)]]
		this.abox = Lazy[mappingAnalysers.fullMatrixBuild[a, b|a.getCrossABoxAxiomsConsistency(b)]]
		this.globalConsistency = Lazy[
			mappingAnalysers.forall[consistency] && (abox.get.flatten.flatten + axioms).consistency
		]
		this.globalRedundancy = Lazy[
			val abx = abox.get
			// println(abx.pretty)
			val r = (0 ..< mappingAnalysers.size)
			r.iterator.pmap [ i |
				val reasoner = r.map[j|if(i != j) abx.get(i).get(j) else axioms].flatten.reasoner
				mappingAnalysers.get(i).aboxAxioms.forall[reasoner.isEntailed(it)]
			].toList.asList
		]
	}

	override getABox() {
		abox.get
	}

	private def pretty(List<List<Set<OWLAxiom>>> lists) {
		'[\n\t' + lists.map[pretty2].join(',\n\t') + '\n]'
	}

	private def pretty2(List<Set<OWLAxiom>> lists) {
		'[\n\t\t' + lists.map[pretty3].join(',\n\t\t') + '\n\t]'
	}

	private def pretty3(Set<OWLAxiom> lists) {
		'[\n\t\t\t' + lists.map[it.toString].join(',\n\t\t\t') + '\n\t\t]'
	}

	private def dispatch getAnalyser(String id) {
		mappingAnalysers.get(id.index)
	}

	private def dispatch getAnalyser(URI uri) {
		mappingAnalysers.get(uri.index)
	}

	private def dispatch int getIndex(String id) {
		mappingAnalysersIndexes.get(id)
	}

	private def dispatch int getIndex(URI id) {
		mappingAnalysersIndexes.get(id.toString)
	}

	private def <T> cell(List<List<T>> matrix, URI uri1, URI uri2) {
		matrix.get(uri1.index).get(uri2.index)
	}

	override boolean getIsSyntacticallyCorrect(URI mapping) {
		mapping.analyser.correction
	}

	override boolean getIsConsistent(URI mapping) {
		mapping.analyser.consistency
	}

	override boolean getIsSubsumedBy(URI mapping1, URI mapping2) {
		headSubsumptionMatrix.get.cell(mapping2, mapping1) && bodySubsumptionMatrix.get.cell(mapping1, mapping2)
	}

	override boolean getIsGloballyConsistent() {
		globalConsistency.get
	}

	override boolean getIsGloballyRedundant(URI mapping) {
		globalRedundancy.get.get(mapping.index)
	}

	override List<HTMLSerializable> getSyntacticResults(URI mapping) {
		mappingAnalysers.get(mapping.index).syntaxResults
	}

	override List<HTMLSerializable> getConsistencyResults(URI mapping) {
		mappingAnalysers.get(mapping.index).consistencyResults
	}

	override List<HTMLSerializable> getSubsumptionResults(URI mapping) {
		getSubsumptionResult(mapping.index)
	}

	override List<HTMLSerializable> getGlobalSubsumptionResults(URI mapping) {
		getGlobalSubsumptionSerializable(mapping.index)
	}

	override Iterable<List<HTMLSerializable>> getSyntacticResults() {
		mappingAnalysers.map[syntaxResults]
	}

	override Iterable<List<HTMLSerializable>> getConsistencyResults() {
		mappingAnalysers.map[consistencyResults]
	}

	override Iterable<List<HTMLSerializable>> getSubsumptionResults() {
		(0 ..< mappingAnalysers.size).map[getSubsumptionResult]
	}

	private def getSubsumptionResult(int i) {
		val is = (0 ..< mappingAnalysers.size)
		val hsm = headSubsumptionMatrix.get
		val bsm = bodySubsumptionMatrix.get
		val phr = is.filter[j|hsm.get(i).get(j)].map[triplesOf(it)].asList
		val HTMLSerializable hr = if(phr.empty) null else new SimpleSerializable(#[triplesOf(i)], headSubsumes, phr)
		val pbr = is.filter[j|bsm.get(i).get(j)].map[triplesOf(it)].asList
		val HTMLSerializable br = if(pbr.empty) null else new SimpleSerializable(#[triplesOf(i)], bodySubsumes, pbr)
		val fsr = is.filter[j|hsm.get(j).get(i) && bsm.get(i).get(j)].map[triplesOf(it)].asList
		val HTMLSerializable fr = if(fsr.empty) null else new SimpleSerializable(#[triplesOf(i)], fullSubsumes, fsr)
		#[hr, br, fr].filter[it != null].asList
	}

	private def triplesOf(int i) {
		mappingAnalysers.get(i).triplesMap
	}

	override Iterable<HTMLSerializable> getGlobalConsistencyResults() {
		val subject = "For the current mapping collection, "
		val HTMLSerializable res = new SimpleSerializable(#[subject], globalInconsistency, #[""])
		if (globalConsistency.get)
			[#[].iterator]
		else
			[#[res].iterator]
	}

	override Iterable<List<HTMLSerializable>> getGlobalSubsumptionResults() {
		(0 ..< mappingAnalysers.size).map[getGlobalSubsumptionSerializable]
	}

	private def List<HTMLSerializable> getGlobalSubsumptionSerializable(int i) {
		val subsumresults = (globalRedundancy.get)
		if (!subsumresults.get(i))
			#[]
		else
			#[new SimpleSerializable(#[triplesOf(i)], globalRedundancyCode, #[])]
	}

/* synchronized override deleteTriplesMap(URI uri) {
 * }
 * 
 * synchronized override addTriplesMap(TriplesMap triplesMap) {
 * }
 * 
 * override modify(TriplesMap triplesMap) {
 * 	addTriplesMap(triplesMap)
 }*/
}

