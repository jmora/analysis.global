package eu.optique.api.component.omm.analysis.global

import com.fluidops.iwb.datasource.metadata.Schema
import eu.optique.api.mapping.TriplesMap
import java.util.Collection
import java.util.List
import java.util.Map
import org.semanticweb.owlapi.model.OWLOntology

import static extension eu.optique.api.component.omm.analysis.global.MappingAnalysisTools.*
import eu.optique.api.component.omm.analysis.syntax.SyntacticAnalyserImpl
import java.io.File
import eu.optique.api.component.omm.analysis.global.MappingAnalysisResult
import eu.optique.api.component.omm.analysis.global.MappingAnalysisResultImpl
import static extension eu.optique.api.component.omm.analysis.global.convenience.Extensions.*

class MappingAnalyser {

	static def MappingAnalysisResult analyse(OWLOntology ontology, Collection<TriplesMap> mappings,
		List<Schema> schemata) {
		new MappingAnalysisResultImpl(ontology, mappings.asList, new SyntacticAnalyserImpl(ontology, schemata),
			schemata.signature.schemaString)
	}

	static def MappingAnalysisResult analyse(OWLOntology ontology, Collection<TriplesMap> mappings,
		Map<String, List<String>> dbsignature, String schemaFilePath) {
		new MappingAnalysisResultImpl(ontology, mappings.asList,
			new SyntacticAnalyserImpl(ontology, new File(schemaFilePath)), schemaFilePath.contents)
	}

	static def MappingAnalysisResult analyse(OWLOntology ontology, Collection<TriplesMap> mappings,
		String schemaFilePath) {
		new MappingAnalysisResultImpl(ontology, mappings.asList,
			new SyntacticAnalyserImpl(ontology, new File(schemaFilePath)), schemaFilePath.contents)
	} 

} 