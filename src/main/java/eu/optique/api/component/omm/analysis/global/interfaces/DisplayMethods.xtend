/**
 * 
 */
package eu.optique.api.component.omm.analysis.global.interfaces

import java.util.List
import eu.optique.api.mapping.TriplesMap
import static extension eu.optique.api.component.omm.analysis.global.MappingAnalysisTools.*

/**
 * @author jmora
 *
 */
class DisplayMethods {
	 
	private static dispatch def toName(TriplesMap tm){
		tm.name
	}
	private static dispatch def toName(Object thing){
		thing.toString
	}
	private static dispatch def toHREF(TriplesMap tm){
		tm.HREF
	}
	private static dispatch def toHREF(Object thing){
		thing.toString
	}
	
	static def <T> asNamed(List<T> things){
		'''{«things.map[toName].join(', ')»}'''
	}
	
	static def <T> asHREF(List<T> things){
		'''{«things.map[toHREF].join(', ')»}'''
	}

	
}