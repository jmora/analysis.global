/**
 * 
 */
package eu.optique.api.component.omm.analysis.global.convenience

import java.util.Map
import java.util.List

/**
 * @author jmora
 * 
 */
class MultiMap<K, V> implements Map<K, V> {

	val List<Pair<K, V>> inner

	new(List<Pair<K, V>> in) {
		inner = in
	}

	override clear() {
		inner.clear
	}

	override containsKey(Object arg0) {
		!inner.filter[key == arg0].isEmpty
	}

	override containsValue(Object arg0) {
		!inner.filter[value == arg0].isEmpty
	}

	override entrySet() {
		inner.iterator.map[new MyEntry(it)].toSet
	}

	override get(Object arg0) {
		inner.iterator.filter[key == arg0].next.value
	}

	override isEmpty() {
		inner.isEmpty
	}

	override keySet() {
		inner.iterator.map[key].toSet
	}

	override put(K arg0, V arg1) {
		inner.add(arg0 -> arg1)
		arg1
	}

	override putAll(Map<? extends K, ? extends V> arg0) {
		inner.addAll(arg0.entrySet.map[key -> value])
	}

	override size() {
		inner.size
	}

	override values() {
		inner.iterator.map[value].toSet
	}
	
	override remove(Object arg0) {
		throw new UnsupportedOperationException("TODO: auto-generated method stub")
	}

}

class MyEntry<K, V> implements Map.Entry<K, V> {
	var Pair<K, V> inner

	new(Pair<K, V> in) {
		inner = in
	}

	synchronized override getKey() {
		inner.key
	}

	synchronized override getValue() {
		inner.value
	}

	synchronized override setValue(V arg0) {
		inner = inner.key -> arg0
		arg0
	}

}