/**
 * 
 */
package eu.optique.api.component.omm.analysis.global.convenience

import java.util.concurrent.Callable

/**
 * @author jmora
 * 
 */
class LazyComputation<T> {

	var pending = true
	final Callable<T> closure
	var T result

	new(Callable<T> computation) {
		closure = computation
	}

	synchronized def get() {
		if (pending) {
			result = closure.call
			pending = false
		}
		result
	}

}