/**
 * 
 */
package eu.optique.api.component.omm.analysis.global.convenience

import eu.optique.api.component.omm.analysis.global.ConstantBuilder
import static extension eu.optique.api.component.omm.analysis.global.convenience.Extensions.*

/**
 * @author jmora
 * 
 */
class Globals {
	static val constantFactory = new ConstantBuilder
	public static val bnodeprefix = "http://http://optique-project.eu/resources/omm/analysis/"
	public static val eager = false
	public static val debugging = true // TODO: change this
	public static final boolean jmora = "C:\\Users\\jmora".exists || "/home/jmora".exists // sorry, I am not allowed to let anyone else use that

	static def getNewConstant() {
		constantFactory.next
	}

}