/**
 * 
 */
package eu.optique.api.component.omm.analysis.global.convenience
import eu.optique.api.component.omm.analysis.impl.bodychecker.BodyChecker
import eu.optique.api.mapping.TriplesMap
import java.util.Map

/**
 * @author jmora
 *
 */
class BodyCheckerStub extends BodyChecker{
	
	override checkBodyConsistency(TriplesMap tm) {
		true
	}
	
	override checkBodySubsumption(TriplesMap tm,TriplesMap  tm2, Map<String,String> uni) {
		true
	}
	
}