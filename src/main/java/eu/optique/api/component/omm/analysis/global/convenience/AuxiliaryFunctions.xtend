/**
 * 
 */
package eu.optique.api.component.omm.analysis.global.convenience

import com.fluidops.iwb.datasource.metadata.impl.InvalidSchemaSpecificationException
import com.fluidops.iwb.datasource.metadata.impl.RelationalMetaDataObjectImpl
import static eu.optique.api.mapping.R2RMLMappingManagerFactory.*
import java.io.File
import java.io.FileInputStream
import org.openrdf.model.impl.URIImpl
import org.openrdf.rio.RDFFormat
import static extension org.openrdf.rio.Rio.*
import static org.semanticweb.owlapi.apibinding.OWLManager.*

import static eu.optique.api.component.omm.analysis.global.convenience.Globals.*
import static eu.optique.api.component.omm.analysis.convenience.Globals.*

import static extension eu.optique.api.component.omm.analysis.global.convenience.AuxiliaryFunctions.*
import org.semanticweb.owlapi.model.OWLAxiom
import org.semanticweb.owlapi.reasoner.InconsistentOntologyException

/**
 * @author jmora
 * 
 */
class AuxiliaryFunctions {
	static def getOntology(String filepath) {
		createOWLOntologyManager.loadOntologyFromOntologyDocument(filepath.file)
	}

	static def getMappings(String filepath) {
		sesameMappingManager.importMappings(filepath.file.graph)
	}

	static def getFile(String s) {
		new File(s)
	}

	static def getGraph(extension File rdfFile) {
		(new FileInputStream(rdfFile)).parse(toURI.toURL.toString,
			absolutePath.getParserFormatForFileName(RDFFormat.TURTLE))
	}

	static def getSchema(String filepath) {
		(new MetaDataObject(bnodeprefix) => [initialiseFromFile(filepath)]).schema
	}

	static def getOntology(Iterable<OWLAxiom> axioms) {
		createOWLOntologyManager.createOntology(axioms.toSet)
	}

	static def getReasoner(Iterable<OWLAxiom> axioms) {
		reasonerFactory.createReasoner(axioms.getOntology) => [precomputeInferences]
	}

	static def getConsistency(Iterable<OWLAxiom> axioms) {
		try {
			axioms.reasoner.isConsistent
		} catch (InconsistentOntologyException ioe) {
			false
		}
	}
}

class MetaDataObject extends RelationalMetaDataObjectImpl {

	new(String metaDataIDString) throws InvalidSchemaSpecificationException {
		super(new URIImpl(metaDataIDString))
	}

	def initialiseFromFile(String filepath) {
		initializeFromGraph(filepath.file.graph, metaDataID)
	}

}
