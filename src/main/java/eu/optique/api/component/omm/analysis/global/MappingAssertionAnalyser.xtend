/**
 * 
 */
package eu.optique.api.component.omm.analysis.global

import eu.optique.api.component.omm.analysis.global.convenience.BodyCheckerAdapter
import eu.optique.api.component.omm.analysis.global.convenience.LazyComputation
import eu.optique.api.component.omm.analysis.interfaces.optique.HTMLSerializable
import eu.optique.api.component.omm.analysis.syntax.SyntacticAnalyserImpl
import eu.optique.api.mapping.ObjectMap
import eu.optique.api.mapping.PredicateObjectMap
import eu.optique.api.mapping.SubjectMap
import eu.optique.api.mapping.TermMap
import eu.optique.api.mapping.TriplesMap
import java.util.List
import java.util.Map
import java.util.Set
import java.util.concurrent.Future
import org.eclipse.xtend.lib.annotations.Accessors
import org.openrdf.model.URI
import org.semanticweb.owlapi.model.IRI
import org.semanticweb.owlapi.model.OWLAxiom
import org.semanticweb.owlapi.model.OWLNamedIndividual

import static eu.optique.api.component.omm.analysis.convenience.Globals.*
import static eu.optique.api.component.omm.analysis.global.convenience.Globals.*
import static eu.optique.api.component.omm.analysis.global.convenience.Statics.*
import static eu.optique.api.component.omm.analysis.global.interfaces.MappingAnalysisCode.*
import static org.semanticweb.owlapi.apibinding.OWLFunctionalSyntaxFactory.*

import static extension eu.optique.api.component.omm.analysis.global.MappingAnalysisTools.*
import static extension eu.optique.api.component.omm.analysis.global.convenience.AuxiliaryFunctions.*
import static extension eu.optique.api.component.omm.analysis.global.convenience.Extensions.*
import org.semanticweb.owlapi.model.OWLOntology

/**
 * @author jmora
 * 
 */
class MappingAssertionAnalyser {
	public val TriplesMap triplesMap
	private extension val BodyCheckerAdapter bodyCheckerAdapter
	private val Map<String, String> unifier

	@Accessors(PUBLIC_GETTER, PROTECTED_SETTER) val Set<OWLAxiom> aboxAxioms
	val Set<OWLAxiom> tboxAxioms

	private val Set<String> ops

	private val List<HTMLSerializable> syntacticResults

	private val LazyComputation<Boolean> headConsistent

	private val LazyComputation<Boolean> bodyConsistent
	
	private val OWLOntology owlOntology

	new(TriplesMap triplesMap, Set<OWLAxiom> axioms, String schema, SyntacticAnalyserImpl syntacticAnalyser,
		Set<String> objectProperties) {
		this.ops = objectProperties
		this.syntacticResults = syntacticAnalyser.analyse(triplesMap).map[it].asList
		this.triplesMap = triplesMap
		this.bodyCheckerAdapter = new BodyCheckerAdapter(schema)
		this.unifier = this.triplesMap.variables.map[it -> newConstant].asMap
		this.aboxAxioms = instantiateHead(unifier).asSet
		this.tboxAxioms = axioms
		this.headConsistent = Lazy[correction && (aboxAxioms + tboxAxioms).consistency]
		this.bodyConsistent = Lazy[correction && bodyCheckerAdapter.getBodyConsistency(triplesMap)]
		this.owlOntology = tboxAxioms.ontology
	}

	def getName() {
		this.triplesMap.name
	}

	def getURI() {
		triplesMap.URI
	}

	def getCorrection() {
		syntacticResults.size == 0
	}

	def getConsistency() {
		headConsistent.get && bodyConsistent.get
	}

	def getSyntaxResults() {
		syntacticResults
	}

	def getConsistencyResults() {
		#[
			(!correction || headConsistent.get) -> headInconsistency,
			(!correction || bodyConsistent.get) -> bodyInconsistency,
			(!correction || (headConsistent.get && bodyConsistent.get)) -> mappingInconsistency
		].filter[!key].map[SerializableResult(#[this.triplesMap], value) as HTMLSerializable].asList
	}

	private def headSubsumes(MappingAssertionAnalyser that) {
		extension val reasoner = (aboxAxioms + tboxAxioms).reasoner
		this.triplesMap.renamings(that.triplesMap).exists [
			that.instantiateHead(it.toMap[value].mapValues[unifier.get(key)]).forall[entailed]
		]
	}

	private def bodySubsumedBy(MappingAssertionAnalyser that) {
		consistency && that.consistency && that.triplesMap.bodySubsumes(triplesMap)
	}

	private def bodySubsumes(MappingAssertionAnalyser that) {
		consistency && that.consistency && triplesMap.bodySubsumes(that.triplesMap)
	}

	def getHeadSubsumption(MappingAssertionAnalyser that) {
		this.headSubsumes(that) -> that.headSubsumes(this)
	}

	def getBodySubsumption(MappingAssertionAnalyser that) {
		that.bodySubsumes(this) -> that.bodySubsumedBy(this)  
	}

	def getCrossABoxAxiomsConsistency(MappingAssertionAnalyser that) {
		val Set<OWLAxiom> res = #{}
		if (!(this.consistency && that.consistency))
			return res -> res
		val r = getCrossABoxAxioms(that)
		if (this === that)
			r.key -> r.key
		else
			r
	}

	private def getCrossABoxAxioms(MappingAssertionAnalyser that) {
		val preunifications = triplesMap.partialUnifications(that.triplesMap).asList
		val pa = preunifications.filter[that.triplesMap.projectionSubsumes(triplesMap, it.asIMap)]
		val a = pa.map[that.instantiateHead(it.toMap[value].mapValues[unifier.get(key)])].flatten.asSet
		val pb = preunifications.filter[triplesMap.projectionSubsumes(that.triplesMap, it.asMap)]
		val b = pb.map[instantiateHead(it.toMap[key].mapValues[that.unifier.get(value)])].flatten.asSet
		a -> b
	}

	// TODO: delete this because it is not really necessary, also change the names to the two methods above
	def getCrossABoxAxiomsRedundancy(MappingAssertionAnalyser that) {
		if (this === that)
			return aboxAxioms -> aboxAxioms
		else
			getCrossABoxAxioms(that)
	}

	/* Start of instantiateHead, all this should be a single line, ideally */
	private def instantiateHead(Map<String, String> unifications) {
		triplesMap.predicateObjectMaps.map[instantiateObjects(it, triplesMap.subjectMap, unifications)].flatten +
			triplesMap.subjectMap.instantiateSubject(unifications)
	}

	private def Iterable<OWLAxiom> instantiateObjects(PredicateObjectMap pomap, TermMap subject,
		Map<String, String> unifications) {
		val s = subject.toIndividual(unifications)
		val ps = pomap.predicateMaps.map[IRI(instantiate(unifications))].asList
		pomap.objectMaps.map[o|ps.map[p|makeTriple(s, p, o, unifications)]].flatten
	}

	private def isObjectProperty(IRI iri) {
		ops.contains(iri.toString)
	}

	private def OWLAxiom makeTriple(OWLNamedIndividual s, IRI p, ObjectMap o, Map<String, String> u) {
		if (p.objectProperty)
			ObjectPropertyAssertion(ObjectProperty(p), s, o.toIndividual(u))
		else{
			val dp = DataProperty(p)
			dp.getRanges(owlOntology).forEach[println(it.toString)]
			DataPropertyAssertion(dp, s, o.toLiteral(u))			
			}
	}

	private def Iterable<OWLAxiom> instantiateSubject(SubjectMap subject, Map<String, String> unifications) {
		subject.getClasses(sesameResource).map[Class(toIRI)].map[ClassAssertion(it, subject.toIndividual(unifications))]
	}

	private def toIndividual(TermMap tm, Map<String, String> unifications) {
		NamedIndividual(IRI(tm.instantiate(unifications)))
	}

	private def instantiate(extension TermMap termMap, Map<String, String> unifications) {
		switch (termMapType) {
			case CONSTANT_VALUED:
				constant
			case COLUMN_VALUED:
				bnodeprefix + (unifications.getOrElse(column, [newConstant]))
			default:
				template.columnNames.fold(template.toString) [ a, b |
					a.replaceAll('''\Q{«b»}\E''', unifications.getOrElse(b, [newConstant]))
				]
		}
	}

	private def toLiteral(TermMap tm, Map<String, String> unifications) {
		// TODO: check actual range of data properties, generate data according to the type
		// if not possible, do not generate
		Literal(tm.instantiate(unifications))
	}

	private def toIRI(URI uri) {
		IRI(uri.toString)
	}

}