/**
 * 
 */
package eu.optique.api.component.omm.analysis.global.convenience

import com.google.common.base.Optional
import java.util.Iterator
import java.util.List

import static eu.optique.api.component.omm.analysis.global.convenience.Statics.*

/**
 * @author jmora
 * 
 */
abstract class Bactracking<T> implements Iterator<T> {

	abstract def Iterator<T> children(T t)

	abstract def Boolean isFinal(T t)

	var Optional<T> head = None

	var Iterator<T> candidates

	new(List<T> candidates) {
		this.candidates = candidates.iterator
		calculateNext
	}

	override hasNext() {
		head.isPresent
	}

	override next() {
		val r = head.get
		calculateNext
		r
	}

	def calculateNext() {
		head = None
		while (candidates.hasNext && !head.isPresent) {
			val c = candidates.next
			if (c.isFinal)
				head = Some(c)
			else 
				candidates = c.children + candidates
		}
	}

}