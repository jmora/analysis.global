package eu.optique.api.component.omm.analysis.global.convenience

import com.google.common.base.Charsets
import com.google.common.collect.ImmutableList
import com.google.common.collect.ImmutableMap
import com.google.common.collect.ImmutableSet
import com.google.common.io.Files
import java.io.File
import java.util.ArrayList
import java.util.Iterator
import java.util.List
import java.util.Map
import java.util.Set
import java.util.concurrent.Callable
import org.eclipse.xtext.xbase.lib.Functions.Function1

import static eu.optique.api.component.omm.analysis.global.convenience.Statics.*

class Extensions {

	static def <I, T> pmap(Iterator<I> input, Function1<I, T> function) {
		input.map[Future[function.apply(it)]].toList.asList.iterator.map[get]
	}

	static def <I, T> pmap(List<I> list, Function1<I, T> function) {
		list.map[Future[function.apply(it)]].asList.map[get]
	}

	static def getDumpFile(String contents) {
		getDumpFile(contents, ".tmp")
	}

	static def getDumpFile(String contents, String ext) {
		val file = File.createTempFile("MappingAnalysis", ext)
		Files.write(contents, file, Charsets.UTF_8);
		file.path
	}

	static def toFile(String filePath) {
		new File(filePath)
	}

	static def <K, V> asMap(Iterable<Pair<K, V>> its) {
		// (new MultiMap<K, V>(its.asList))
		its.toMap[key].mapValues[value].immmap
	}

	static def <K, V> asIMap(Iterable<Pair<K, V>> its) {
		// new MultiMap<V, K>(its.map[value -> key].asList)
		its.toMap[value].mapValues[key].immmap
	}

	static def exists(String path) {
		Maybe[new File(path)]?.exists
	}

	static def contents(String path) {
		Files.toString(new File(path), Charsets.UTF_8);
	}

	static def <A, B> zip(List<A> first, List<B> second) {
		(0 ..< Math.min(first.size, second.size)).map[first.get(it) -> second.get(it)]
	}

	static def <A, B> getOrElse(Map<A, B> map, A key, Callable<B> f) {
		if (map.containsKey(key))
			map.get(key)
		else
			f.call
	}

	static def <A> List<A> asList(Iterable<A> iter) {
		ImmutableList.copyOf(iter)
	}

	static def <A> List<A> toArrayList(Iterable<A> iter) {
		new ArrayList<A>(iter.toList)
	}
	
	static def <A> ArrayList<A> asArray(Iterable<A> iter) {
		new ArrayList<A>(iter.toList)
	}

	static def <A> Set<A> asSet(Iterable<A> iter) {
		ImmutableSet.copyOf(iter)
	}

	static def <K, V> Map<K, V> immmap(Map<K, V> mmap) {
		ImmutableMap.copyOf(mmap)
	}

	static def <A> headOrElse(Iterable<A> collection, A alternative) {
		val iter = collection.iterator
		if(iter.hasNext) iter.next else alternative
	}

	static def <K, V> invert(Map<K, V> map) {
		map.entrySet.map[value -> key].asMap
	}

}