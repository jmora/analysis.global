/**
 * 
 */
package eu.optique.api.component.omm.analysis.global.convenience

import org.eclipse.xtend.lib.annotations.Data

import static eu.optique.api.component.omm.analysis.global.MappingAnalyser.*

import static extension eu.optique.api.component.omm.analysis.global.MappingAnalysisTools.*
import static extension eu.optique.api.component.omm.analysis.global.convenience.AuxiliaryFunctions.*
import static extension eu.optique.api.component.omm.analysis.global.convenience.Extensions.*
import eu.optique.api.mapping.TriplesMap
import com.google.common.collect.Ordering

/**
 * @author jmora
 * 
 */
class TestRunner {
	static val base = 'test/resources/global/redundancy/'
	private static val Ordering<TriplesMap> order = [a, b|a.URI.compareTo(b.URI)]

	def static void main(String[] args) { 
//		#[
//			new TestCase("TGCN1", "map1-inconsistent.ttl", "database.schema", "person-ontology.ttl", false),
//			new TestCase("TGCN2", "map2-inconsistent.ttl", "database.schema", "person-ontology.ttl", false),
//			new TestCase("TGCP", "map1-consistent-redundant.ttl", "database.schema", "person-ontology.ttl", true),
//			new TestCase("TGCNRoles1", "map3-inconsistent.ttl", "database2.schema", "ontology2.ttl", false),
//			new TestCase("TGCNRoles2", "map4-inconsistent.ttl", "database2.schema", "ontology2.ttl", false),
//			new TestCase("TGCNRoles3", "map5-inconsistent.ttl", "database2.schema", "ontology2.ttl", false),
//			new TestCase("TGCNRoles4", "map6-inconsistent.ttl", "database2.schema", "ontology2.ttl", false),
//			new TestCase("TGCPRoles1", "map3-consistent.ttl", "database2.schema", "ontology2.ttl", true),
//			new TestCase("TGCPRoles2", "map4-consistent.ttl", "database2.schema", "ontology2.ttl", true),
//			new TestCase("TGCNAttributes1", "map7-inconsistent.ttl", "database2.schema", "ontology3.ttl", false),
//			new TestCase("TGCNAttributes2", "map8-inconsistent.ttl", "database2.schema", "ontology3.ttl", false),
//			new TestCase("TGCPAttributes1", "map5-consistent.ttl", "database2.schema", "ontology3.ttl", true),
//			new TestCase("TGCNAttributesRoles1", "map9-inconsistent.ttl", "database2.schema", "ontology4.ttl", false),
//			new TestCase("TGCNAttributesRoles2", "map10-inconsistent.ttl", "database2.schema", "ontology4.ttl", false),
//			new TestCase("TGCPAttributesRoles1", "map6-consistent.ttl", "database2.schema", "ontology4.ttl", true)
//		// ].map[testGlobalConsistency].map[println(it)].asList.forEach[println(it)]
//		].map[testGlobalConsistency].forEach[println(it)]
	 #[
	 new RedundancyTestCase("C2M2", "map2-redundant.ttl", "database.schema", "person-ontology.ttl", 1, true)
	 ].map[testGlobalRedundancy].forEach[println(it)]
	}

	static def testGlobalConsistency(extension TestCase currentTestCase) {
		val start = System.currentTimeMillis
		val result = analyse((base + ontologyPath).ontology, (base + mappingsPath).mappings, base + schemaPath)
		new TestCaseResult(name.expand, System.currentTimeMillis - start, result.isGloballyConsistent == expected)
	}

	static def testGlobalRedundancy(extension RedundancyTestCase currentTestCase) {
		val maps = order.sortedCopy((base + mappingsPath).mappings)
		val start = System.currentTimeMillis
		val result = analyse((base + ontologyPath).ontology, maps, base + schemaPath)
		new TestCaseResult(name.expand, System.currentTimeMillis - start,
			result.getIsGloballyRedundant(maps.get(about).URI) == expected)
	}

	static def expand(String name) {
		name.replace('TGCP', 'TestGlobalConsistencyPositive').replace('TGCN', 'TestGlobalConsistencyNegative')
	}

}

@Data class TestCase {
	String name
	String mappingsPath
	String schemaPath
	String ontologyPath
	boolean expected
}

@Data class RedundancyTestCase {
	String name
	String mappingsPath
	String schemaPath
	String ontologyPath
	int about
	boolean expected
}

@Data class TestCaseResult {
	String name
	Long time
	boolean worked

	override def toString() {
		'''«if (worked) '-' else ' !'»	«name»	ran in «time»ms,	«if(worked) 'successfully' else 'and failed'»'''
	}
}
	