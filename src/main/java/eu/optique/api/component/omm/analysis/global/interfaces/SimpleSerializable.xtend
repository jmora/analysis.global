/**
 * 
 */
package eu.optique.api.component.omm.analysis.global.interfaces

import eu.optique.api.component.omm.analysis.interfaces.optique.HTMLSerializable
import java.util.List
import org.eclipse.xtend.lib.annotations.Data

import static extension eu.optique.api.component.omm.analysis.global.interfaces.DisplayMethods.*

/**
 * @author jmora
 * 
 */
@Data class SimpleSerializable<T, K> implements HTMLSerializable {
	val List<T> subject
	val MappingAnalysisCode predicate
	val List<K> object

	override toHTML() {
		subject.asHREF + predicate.toString + object.asHREF
	}

	override toNamedString() {
		subject.asNamed + predicate.toString + object.asNamed
	}

}
	
