package eu.optique.api.component.omm.analysis.global.convenience

import com.google.common.base.Optional
import eu.optique.api.component.omm.analysis.global.interfaces.MappingAnalysisCode
import eu.optique.api.component.omm.analysis.global.interfaces.SimpleSerializable
import java.util.List
import java.util.concurrent.Callable
import org.eclipse.xtext.xbase.lib.Functions.Function1

import static java.lang.Runtime.*
import static java.util.concurrent.Executors.*

class Statics {

	static def <T> None() {
		Optional.<T>absent
	}

	static def <T> Some(T thing) {
		Optional.of(thing)
	}

	private static val pool = newFixedThreadPool(runtime.availableProcessors)[new Thread(it) => [daemon = true]]

	static def <T> Future(Callable<T> callable) {
		pool.submit(callable)
	}

	static def <I, T> Future(I i, Function1<I, T> f) {
		pool.submit[f.apply(i)]
	}

	static def <A> A tryOrElse(Callable<A> closure, Callable<A> alternative) {
		try {
			closure.call
		} catch (Exception e) {
			alternative.call
		}
	}

	static def <A> A Maybe(Callable<A> closure) {
		try {
			closure.call
		} catch (Exception e) {
			e.printStackTrace
			null
		}
	}

	static def <T> Lazy(Callable<T> f) {
		new LazyComputation(f)
	}

	static def <K, V> SerializableResult(List<K> s, MappingAnalysisCode p, List<V> o) {
		new SimpleSerializable<K, V>(s, p, o)
	}

	static def <K> SerializableResult(List<K> s, MappingAnalysisCode p) {
		new SimpleSerializable<K, String>(s, p, #[])
	}

}