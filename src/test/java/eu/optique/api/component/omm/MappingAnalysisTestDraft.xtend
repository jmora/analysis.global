/**
 * 
 */
package eu.optique.api.component.omm

import static extension eu.optique.api.component.omm.analysis.global.MappingAnalysisTools.*
import static extension eu.optique.api.component.omm.analysis.global.convenience.AuxiliaryFunctions.*
import static eu.optique.api.component.omm.analysis.global.convenience.Statics.*
import static extension eu.optique.api.component.omm.analysis.global.convenience.Extensions.*
import eu.optique.api.component.omm.analysis.global.MappingAnalyser

/**
 * @author jmora
 *
 */
class MappingAnalysisTestDraft {
	public static def runTest(){
		val mappingsPath = ""
		val schemaPath = ""
		val ontologyPath = ""
		val ontology = ontologyPath.ontology
		val mappings = mappingsPath.mappings
		val result = MappingAnalyser.analyse(ontology, mappings, schemaPath)
		System.out.println("The mappings are globally consistent: " + result.isGloballyConsistent)
		for (m : mappings){
			System.out.println("The mapping " + m.name + "is globally redundant: " +  result.getIsGloballyRedundant(m.URI))
		}
	}
	
}