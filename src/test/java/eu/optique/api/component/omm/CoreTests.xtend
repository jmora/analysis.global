/**
 * 
 */
package eu.optique.api.component.omm

import org.junit.Test
import static extension eu.optique.api.component.omm.analysis.global.convenience.AuxiliaryFunctions.*
import static extension eu.optique.api.component.omm.analysis.global.MappingAnalysisTools.*
import static org.junit.Assert.*
import java.net.URI
import static org.semanticweb.owlapi.apibinding.OWLFunctionalSyntaxFactory.*

/**
 * @author jmora
 * 
 * Tests for small complex stuff that I run to see if I got things right 
 * 
 */
class CoreTests {

	def checkURIs() {
		val mappings = "resources/mappings.ttl".mappings
		mappings.forEach[println(it.URI)]
		assertTrue(true)
	}

	@Test
	def checkRanges() {
		val r = (0 ..< 10)
		r.forEach[i|r.forEach[j|println('''«i»«j»''')]]
	}

	@Test
	def checkSyntax() {
		IRI("http://www.semanticweb.org/optique/test-ontology#Father/John")
		URI.create("http://www.semanticweb.org/optique/test-ontology#Father/John")
		assertTrue(true)
	}

}