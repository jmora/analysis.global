/**
 * 
 */
package eu.optique.api.component.omm

import com.google.common.collect.Ordering
import eu.optique.api.component.omm.analysis.global.convenience.BodyCheckerAdapter
import eu.optique.api.mapping.TriplesMap
import java.util.Map
import org.junit.Test

import static org.junit.Assert.*

import static extension eu.optique.api.component.omm.analysis.global.MappingAnalysisTools.*
import static extension eu.optique.api.component.omm.analysis.global.convenience.AuxiliaryFunctions.*
import static extension eu.optique.api.component.omm.analysis.global.convenience.Extensions.*

/**
 * @author jmora
 * 
 */
class BodyCheckerTest {
	static val base = 'test/resources/global/redundancy/'
	static val Ordering<TriplesMap> order = [a, b|a.URI.compareTo(b.URI)]

	@Test
	def TestGlobalConsistencyTestConcepts2Mapping2() {
		val result = obtainResult("map2-redundant.ttl", "database.schema", #{"name" -> "nome"});
		assertTrue(true -> false == result)
	}

	@Test
	def TestGlobalRedundancyAttributes2Mapping2() {
		val result = obtainResult("map6-redundant.ttl", "database2.schema", #{"column1" -> "x", "column2" -> "y"});
		assertTrue(true -> false == result)
	}

	def obtainResult(String mappingsPath, String schemaPath, Map<String, String> unification) {
		val mappings = order.sortedCopy((base + mappingsPath).mappings)
		val bc = new BodyCheckerAdapter((base + schemaPath).contents)
		bc.projectionSubsumes(mappings.get(0), mappings.get(1), unification) ->
			bc.projectionSubsumes(mappings.get(1), mappings.get(0), unification.invert)
	}
}