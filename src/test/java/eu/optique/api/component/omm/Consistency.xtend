/**
 * 
 */
package eu.optique.api.component.omm

import org.junit.Test
import static extension eu.optique.api.component.omm.analysis.global.convenience.AuxiliaryFunctions.*
import static org.junit.Assert.*
import static eu.optique.api.component.omm.analysis.global.MappingAnalyser.*

/**
 * @author jmora
 * 
 */
class Consistency {
	val base = 'test/resources/global/'

	@Test
	def TestGlobalConsistencyNegative1() {
		testGlobalConsistency("map1-inconsistent.ttl", "database.schema", "person-ontology.ttl", false)
	}

	@Test
	def TestGlobalConsistencyNegative2() {
		testGlobalConsistency("map2-inconsistent.ttl", "database.schema", "person-ontology.ttl", false)
	}

	@Test
	def TestGlobalConsistencyPositive() {
		testGlobalConsistency("map1-consistent-redundant.ttl", "database.schema", "person-ontology.ttl", true)
	}

	@Test
	def TestGlobalConsistencyNegativeRoles1() {
		testGlobalConsistency("map3-inconsistent.ttl", "database2.schema", "ontology2.ttl", false)
	}

	@Test
	def TestGlobalConsistencyNegativeRoles2() {
		testGlobalConsistency("map4-inconsistent.ttl", "database2.schema", "ontology2.ttl", false)
	}

	@Test
	def TestGlobalConsistencyNegativeRoles3() {
		testGlobalConsistency("map5-inconsistent.ttl", "database2.schema", "ontology2.ttl", false)
	}

	@Test
	def TestGlobalConsistencyNegativeRoles4() {
		testGlobalConsistency("map6-inconsistent.ttl", "database2.schema", "ontology2.ttl", false)
	}

	@Test
	def TestGlobalConsistencyPositiveRoles1() {
		testGlobalConsistency("map3-consistent.ttl", "database2.schema", "ontology2.ttl", true)
	}

	@Test
	def TestGlobalConsistencyPositiveRoles2() {
		testGlobalConsistency("map4-consistent.ttl", "database2.schema", "ontology2.ttl", true)
	}

	@Test
	def TestGlobalConsistencyNegativeAttributes1() {
		testGlobalConsistency("map7-inconsistent.ttl", "database2.schema", "ontology3.ttl", false)
	}

	@Test
	def TestGlobalConsistencyNegativeAttributes2() {
		testGlobalConsistency("map8-inconsistent.ttl", "database2.schema", "ontology3.ttl", false)
	}

	@Test
	def TestGlobalConsistencyPositiveAttributes1() {
		testGlobalConsistency("map5-consistent.ttl", "database2.schema", "ontology3.ttl", true)
	}

	@Test
	def TestGlobalConsistencyNegativeAttributesRoles1() {
		testGlobalConsistency("map9-inconsistent.ttl", "database2.schema", "ontology4.ttl", false)
	}

	@Test
	def TestGlobalConsistencyNegativeAttributesRoles2() {
		testGlobalConsistency("map10-inconsistent.ttl", "database2.schema", "ontology4.ttl", false)
	}

	@Test
	def TestGlobalConsistencyPositiveAttributesRoles1() {
		testGlobalConsistency("map6-consistent.ttl", "database2.schema", "ontology4.ttl", true)
	}

	def testGlobalConsistency(String mappingsPath, String schemaPath, String ontologyPath, boolean expected) {
		val result = analyse((base + ontologyPath).ontology, (base + mappingsPath).mappings, base + schemaPath)
		assertTrue(result.isGloballyConsistent == expected)
	}

}