/**
 * 
 */
package eu.optique.api.component.omm

import static extension eu.optique.api.component.omm.analysis.global.MappingAnalysisTools.*
import static extension eu.optique.api.component.omm.analysis.global.convenience.AuxiliaryFunctions.*
import static eu.optique.api.component.omm.analysis.global.convenience.Statics.*
import static extension eu.optique.api.component.omm.analysis.global.convenience.Extensions.*
import eu.optique.api.component.omm.analysis.global.MappingAnalyser

/**
 * @author jmora
 * 
 */
class MappingsReport {
	public static def main(String[] args) {
		val mappingsPath = "test/resources/specification/demoMappings.ttl"
		val schemaPath = "test/resources/specification/demoSources.schema"
		val ontologyPath = "test/resources/specification/demoOntology.ttl"
		val ontology = ontologyPath.ontology
		val mappings = mappingsPath.mappings
		val result = MappingAnalyser.analyse(ontology, mappings, schemaPath)
		println("Syntactic problems:")
		result.syntacticResults.forEach[println(it)]
		println("\n\nBody consistency problems:")
		result.consistencyResults.forEach[println]
	}
}